use eframe::{egui, Frame};
use eframe::egui::{Align, Context, Key, Layout, ScrollArea, TextEdit, Ui, Rounding, TextStyle, FontFamily, FontId};
use eframe::egui::FontFamily::Proportional;
use eframe::egui::TextStyle::{Small, Monospace, Body, Heading};
use eframe::emath::Vec2;
use egui::vec2;
use egui_extras::{Size, StripBuilder};
use crate::about::AboutWindow;

#[derive(Debug)]
struct TodoEntry {
    id: u64,
    text: String,
}

impl TodoEntry {
    pub fn new(id: u64, text: impl AsRef<str>) -> Self {
        Self {
            id, text: text.as_ref().to_string(),
        }
    }
}

pub struct App {
    initial: bool,
    about: AboutWindow,
    items: Vec<TodoEntry>,
    ids_to_remove: Vec<u64>,
    line_edit: String,
    next_id: u64,
    quit: bool
}

impl App {
    #[cfg(not(target_arch = "wasm32"))]
    fn init_styles(cc: &eframe::CreationContext) {
        let mut style = egui::Style::default();

        style.text_styles = [
            (Heading, FontId::new(14.0, Proportional)),
            (Body, FontId::new(12.0, Proportional)),
            (Monospace, FontId::new(12.0, FontFamily::Monospace)),
            (TextStyle::Button, FontId::new(13.0, Proportional)),
            (Small, FontId::new(10.0, Proportional)),
        ].into();
        cc.egui_ctx.set_style(style);

        let mut visuals = egui::Visuals::light();
        visuals.window_rounding = Rounding::from(1.0);
        visuals.window_shadow.extrusion = 4.0;
        cc.egui_ctx.set_visuals(visuals);
    }

    #[cfg(target_arch = "wasm32")]
    fn init_styles(cc: &eframe::CreationContext) {

    }
//
    pub fn new(cc: &eframe::CreationContext) -> Self {
        Self::init_styles(cc);

        App {
            initial: false,
            about: AboutWindow::create(),
            items: vec![
                TodoEntry::new(0, "Learn Haskell"),
                TodoEntry::new(1, "Learn Erlang"),
                TodoEntry::new(2, "Learn Elixir"),
            ],
            ids_to_remove: vec![],
            line_edit: "(enter text here)".into(),
            next_id: 2u64,
            quit: false
        }
    }

    fn reset(&mut self) {
        self.quit = false;
    }

    fn build_top(&mut self, ui: &mut Ui) {
        ui.visuals_mut().button_frame = false;
        ui.horizontal(|ui| {
            ui.menu_button("Todo", |ui| {
                if ui.button("Clear").clicked() {
                    self.items.clear();
                    ui.close_menu();
                }

                #[cfg(not(target_arch = "wasm32"))]
                if ui.button("Exit").clicked() {
                    self.quit = true;
                    ui.close_menu();
                }
            });
            ui.menu_button("Help", |ui| {
                if ui.button("About").clicked() {
                    self.about.visible = true;
                    ui.close_menu();
                }
            });
        });
    }

    fn new_id(&mut self) -> u64 {
        self.next_id += 1;
        self.next_id
    }

    fn build_center(&mut self, ui: &mut Ui) {
        use egui_extras::{TableBuilder};

        ui.visuals_mut().collapsing_header_frame = true;

        ui.vertical(|ui| {
            StripBuilder::new(ui)
                .size(Size::remainder())
                .size(Size::exact(30.0))
                .vertical(|mut strip| {
                    strip.cell(|ui| {
                        ScrollArea::horizontal().show(ui, |ui| {
                            TableBuilder::new(ui)
                                .cell_layout(Layout::left_to_right(egui::Align::Center))
                                .column(Size::remainder().at_least(100.0))
                                .striped(true)
                                .body(|mut body| {
                                    body.rows(18.0, self.items.len(), |index, mut row| {
                                        row.col(|ui| {
                                            let caption = format!(
                                                "[{}/{}] {}",
                                                index + 1, self.items.len(),
                                                &self.items[index].text);

                                            if ui.button("❌").on_hover_text("Remove this entry").clicked() {
                                                self.ids_to_remove.push(self.items[index].id);
                                            }

                                            ui.label(caption).on_hover_text(
                                                format!("Index: {}\nId: {}\nMem ptr: 0x{:016x}",
                                                        index,
                                                        self.items[index].id,
                                                        &self.items[index] as *const TodoEntry as u64,
                                                )
                                            );
                                        });
                                    });
                                });
                        });
                    });

                    strip.cell(|ui| {
                        ui.add_space(2.0);
                        ui.separator();

                        ui.with_layout(Layout::right_to_left(Align::Center), |ui| {
                            let mut add = false;
                            let mut focused = false;

                            if ui.button("Add").clicked() {
                                add = true;
                            }

                            let te = TextEdit::singleline(&mut self.line_edit);
                            let re = ui.add_sized(ui.available_size(), te);

                            if re.lost_focus() && re.ctx.input().key_pressed(Key::Enter) {
                                add = true;
                            }

                            if add {
                                let id = self.new_id();
                                self.items.push(TodoEntry::new(id, &self.line_edit));
                                self.line_edit = "".into();
                                re.request_focus();
                            }
                        });
                    });
                });
        });
    }
}

impl eframe::App for App {
    fn update(&mut self, ctx: &Context, frame: &mut Frame) {
        if !self.initial {
            #[cfg(not(target_arch = "wasm32"))]
            frame.set_window_title("TODO app");

            self.initial = true;
        }

        egui::TopBottomPanel::top("top").show(ctx, |ui| self.build_top(ui));
        egui::CentralPanel::default().show(ctx, |ui| self.build_center(ui));

        self.about.update(ctx);

        if self.quit {
            #[cfg(not(target_arch = "wasm32"))]
            frame.close();
        }

        for rid in &self.ids_to_remove {
            for i in 0..self.items.len() {
                if self.items[i].id == *rid {
                    self.items.remove(i);
                    break;
                }
            }
        }

        self.ids_to_remove.clear();
    }
}
