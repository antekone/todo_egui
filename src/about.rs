use eframe::egui;
use eframe::egui::{TextBuffer, Ui, vec2};

pub struct AboutWindow {
    pub visible: bool,
    buf: String,
}

impl AboutWindow {
    pub fn create() -> Self {
        Self {
            visible: false,
            buf: "Just a simple proof of concept tool implementing a \
                primitive TODO app.\n\n\
                Tools used: Rust + egui\n\n\
                Written by @antekone".to_string(),
        }
    }

    pub fn update(&mut self, ctx: &egui::Context) {
        if !self.visible { return; }

        egui::Window::new("About")
            .collapsible(false)
            .anchor(egui::Align2::CENTER_TOP, vec2(0.0, 30.0))
            .title_bar(false)
            .default_size(vec2(240.0, 120.0))
            .show(ctx, |ui| { self.build(ui); });
    }

    fn build(&mut self, ui: &mut Ui) {
        ui.vertical_centered(|ui| {
            ui.heading("TODO App");
            ui.separator();

            ui.vertical_centered_justified(|ui| {
                ui.add_enabled_ui(false, |ui| {
                    ui.text_edit_multiline(&mut self.buf);
                });
            });

            ui.separator();
            if ui.button("👍 Close").clicked() {
                self.visible = false;
            }
        });
    }
}