#![warn(clippy::all, rust_2018_idioms)]

mod app;
mod about;

pub use app::App;
